import java.util.*

fun main(args: Array<String>) {

    /*val orderedDeck = Deck()
    val randomDeck = Deck()
    var acc = 0
    val trials = 100000

    for (i in 1..trials) {
        randomDeck.randomShuffle()
        acc += randomDeck.similar(orderedDeck)
    }
    println((acc.toFloat() / trials).toString())*/

//    val random = Random()
//    random.setSeed(Date().time)
//    var max = 0
//    var min = 52
//    for (i in 1..10000) {
//        val j = random.nextInt(52 - 1) + 1
//        if (j > max) {
//            max = j
//        }
//        if (j < min) {
//            min = j
//        }
//    }
//    println("Min = $min, Max = $max")

    val deck = Deck()
//    deck.cutShuffle(100)
//    println(deck.toString())
}

enum class Suits(val order: Int, val color: Color, val symbol: String) {
    Spades(1, Color.BLACK, "♠"),
    Hearts(2, Color.RED, "♥"),
    Clubs(3, Color.BLACK, "♣"),
    Diamonds(4, Color.RED, "♦")
}

enum class Color {
    RED, BLACK
}

enum class Ranks(val value: Int, val symbol: String) {
    Ace(1, "1"),
    Two(2, "2"),
    Three(3, "3"),
    Four(4, "4"),
    Five(5, "5"),
    Six(6, "6"),
    Seven(7, "7"),
    Eight(8, "8"),
    Nine(9, "9"),
    Ten(10, "X"),
    Jack(11, "J"),
    Queen(12, "Q"),
    King(13, "K")
}

data class Card(val rank: Ranks, val suit: Suits) {

    override fun toString(): String {
        return rank.symbol + suit.symbol
//        return toInteger().toString()
    }

    fun toInteger(): Int {
        return (suit.order - 1) * 13 + rank.value
    }
}

class Deck() {

    private val deckSize = 52

    val cards: Array<Card> = Array(deckSize, ::integerToCard)

    /**
     * Performs a Fisher-Yates shuffle of the deck.
     *
     * If the random number generator is perfect, then the resulting deck is perfectly randomly shuffled.
     *
     * @return This deck for chaining
     */
    fun randomShuffle(): Deck {
        val random = Random()
        random.setSeed(Date().time)

        for (i in 0..(cards.size - 1)) {
            cards.swap(i, random.nextInt(i))
        }
        return this
    }

    /**
     * Shuffles the deck by cutting it at a certain position and swapping the two halves
     *
     * Repeats the process [iterations] number of items.
     * Pointless, since the cards will always be in order, just with different start/end points.
     *
     * @return This deck for chaining
     */
    fun cutShuffle(iterations: Int): Deck {
        if (iterations < 1) throw Exception("'iterations' must be greater than 1. Received: $iterations")
        val random = Random()
        random.setSeed(Date().time)
        for (i in 1..iterations) {
            val cardAfterCutPosition = random.nextInt(deckSize - 1) + 1
            println("Cut right before card at index $cardAfterCutPosition")
            cutAndSwap(cardAfterCutPosition)
            println(toString())
        }
        return this
    }

    /**
     * Swaps the two halves of the deck.
     *
     * `1, 2, ... cardAfterCutPosition - 1, cardAfterCutPosition, cardAfterCutPosition + 1, ... 52`
     *
     * becomes
     *
     * `cardAfterCutPosition, cardAfterCutPosition + 1, ... 52, 1, 2, ... cardAfterCutPosition - 1`
     *
     */
    private fun cutAndSwap(cardAfterCutPosition: Int) {
        val firstHalfTemp = cards.copyOfRange(0, cardAfterCutPosition)
        val secondHalfTemp = cards.copyOfRange(cardAfterCutPosition, cards.size)
        for (i in 0..secondHalfTemp.size - 1) {
            cards[i] = secondHalfTemp[i]
        }
        for (i in 0..firstHalfTemp.size - 1) {
            cards[i + secondHalfTemp.size] = firstHalfTemp[i]
        }
    }

    override fun toString(): String {
        var returnVal: String = ""

        var i = 1
        for (card in cards) {
            returnVal += card.toString()
            if (i in listOf(13, 26, 39)) {
                returnVal += "\n"
            }
            else if (i != 52) {
                returnVal += " "
            }
            i++
        }

        return returnVal
    }

    fun similar(other: Deck): Int {
        val array = BooleanArray(deckSize)
        for (i in 0..(cards.size - 1)) {
            array[i] = this[i] == other[i]
        }
        return array.count { it == true }
    }

    operator fun get(i: Int): Card {
        return cards[i]
    }
}

/** Uses the suit order Spades, Hearts, Clubs, Diamonds, so a [value] of 0 returns an Ace of Spades, and a value of 51 returns a King of Diamonds */
fun integerToCard(value: Int): Card {
    if (value !in 0..51) throw Exception("Parameter 'value' must be between 0 and 51 inclusive, received: $value")
    return Card(Ranks.values()[(value % 13)], Suits.values()[value / 13])
}

fun <T> Array<T>.swap(firstPosition: Int, secondPosition: Int) {
    val temp = this[firstPosition]
    this[firstPosition] = this[secondPosition]
    this[secondPosition] = temp
}